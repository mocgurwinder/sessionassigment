package com.example.constants


object Constants {

    object ErrorIdentityCode {
        var sVALIDATION_ERROR = 0
        var sAPI_ERROR = 1
        var sOTHER_ERROR = 2
        var sEMPTY_LIST = 3
        var sEMPTYSEARCH_LIST = 4
    }
}
