package com.example.sessionassigment.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sessionassigment.R
import com.example.sessionassigment.adapters.RecycleViewAdapter
import com.example.sessionassigment.restapimodels.response.SessionResponse
import com.example.sessionassigment.viewmodels.MainViewModel
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private var mSelectedItemPriceList = ArrayList<JsonObject>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.mPresenter.getSessionFromApi()
        viewModel.mMutableList.observe(viewLifecycleOwner, object : Observer<SessionResponse> {
            override fun onChanged(t: SessionResponse?) {
                if (!t?.pSessionList?.pTodaysSessions.isNullOrEmpty()) {
                    mSelectedItemPriceList.addAll(t?.pSessionList?.pTodaysSessions!!)
                    setAdapter()
                    tv_error.visibility = View.GONE
                } else {
                    tv_error.visibility = View.VISIBLE
                    tv_error.text = t?.pSessionList?.pMessageDescription!!
                }
            }
        })
        setAdapter()
    }

    private fun setAdapter() {
        recycle_view.layoutManager = LinearLayoutManager(requireContext())
        recycle_view.adapter = RecycleViewAdapter(mSelectedItemPriceList,context)
    }
}
