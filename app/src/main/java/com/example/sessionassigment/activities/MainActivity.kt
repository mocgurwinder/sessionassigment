package com.example.sessionassigment.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.application.Applications
import com.example.sessionassigment.R
import com.example.sessionassigment.fragments.MainFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        Applications.mCurrentInstance?.setActivity(this)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().replace(R.id.container, MainFragment.newInstance()).commitNow()
        }
    }
}
