package com.example.sessionassigment.adapters

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.item_view_layout.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class RecycleViewAdapter(private val pItemModel: ArrayList<JsonObject>,
                         context: Context?
) : RecyclerView.Adapter<ViewHolder>() {
    val mItemModel=pItemModel
    private var alarmMgr: AlarmManager? = null
    private lateinit var alarmIntent: PendingIntent
    private var context: Context? = context;

    override fun getItemCount(): Int {
        return mItemModel.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(com.example.sessionassigment.R.layout.item_view_layout, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
      holder.mItemView.tv_firstname.text=  mItemModel[position].get("first_name").asString
      holder.mItemView.tv_lastname.text= mItemModel[position].get("last_name").asString
      holder.mItemView.tv_sessionTime.text=  mItemModel[position].get("session_time").asString
      holder.mItemView.tv_status.text=  mItemModel[position].get("status").asString
      holder.mItemView.tv_firstname.text=  pItemModel[position].get("first_name").asString
      holder.mItemView.tv_lastname.text= pItemModel[position].get("last_name").asString
      holder.mItemView.tv_sessionTime.text=  pItemModel[position].get("session_time").asString
      holder.mItemView.tv_status.text=  pItemModel[position].get("status").asString
      //  if(pItemModel[position].get("status").asString.equals("Not Started"))
        if(position==0)
        fireAlarm(pItemModel[position].get("session_time").asString)
       // fireAlarm("02:15:00")

    }

    @SuppressLint("SimpleDateFormat")
    fun localToGMT(pValue: String): Date {
        val date = Date()
        val sdf = SimpleDateFormat("HH:mm:ss")
        sdf.parse(pValue)
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"))
        return Date(sdf.format(date))
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun fireAlarm(asString: String?) {
       // println(time)
        val dateFormatter = SimpleDateFormat("HH:mm:ss", Locale.ENGLISH)
        try {
            val c = Calendar.getInstance()
            c.time = dateFormatter.parse(asString)
            val hour = c.get(Calendar.HOUR_OF_DAY)
            val minute = c.get(Calendar.MINUTE)
            val setcalendar = Calendar.getInstance()
            setcalendar.setTimeInMillis(System.currentTimeMillis())
            setcalendar.set(Calendar.HOUR_OF_DAY, hour)
            setcalendar.set(Calendar.MINUTE, minute + 4)

        alarmMgr = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmIntent = Intent(context, com.example.sessionassigment.AlarmManager::class.java).let { intent ->
            PendingIntent.getBroadcast(context, 0, intent, 0)
        }

        alarmMgr?.set(
            AlarmManager.RTC_WAKEUP,setcalendar.timeInMillis,
            alarmIntent
        )
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }


}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val mItemView = view
}