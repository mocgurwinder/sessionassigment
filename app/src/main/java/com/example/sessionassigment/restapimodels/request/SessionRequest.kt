package com.example.sessionassigment.restapimodels.request

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SessionRequest(
    @SerializedName("entity_id") var pEntity_Id: Int = 31,
    @SerializedName("day") var pDay: Int = 1
) : Serializable
