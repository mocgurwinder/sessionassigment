package com.example.sessionassigment.restapimodels.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SessionResponse(
    @SerializedName("status")       var pStatus: String,
    @SerializedName("responseCode") var pResponseCode: String,
    @SerializedName("data")         var pSessionList: Sessions
):Serializable