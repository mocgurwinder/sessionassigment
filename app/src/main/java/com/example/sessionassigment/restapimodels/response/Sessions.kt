package com.example.sessionassigment.restapimodels.response

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Sessions(
    @SerializedName("todaysSessions") var pTodaysSessions:ArrayList<JsonObject>,
    @SerializedName("messageDescription") var pMessageDescription:String
):Serializable