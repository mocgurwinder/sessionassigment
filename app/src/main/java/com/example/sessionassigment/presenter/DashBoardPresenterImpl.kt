package com.example.sessionassigment.presenter

import android.util.Log
import com.example.application.Applications
import com.example.basePresenter.MVPView
import com.example.constants.Constants
import com.example.dashBoard.restApiResponse.LogOutNetworkApi
import com.example.networkUtil.apiConfig.ApiClientConfig
import com.example.sessionassigment.restapimodels.request.SessionRequest
import com.example.sessionassigment.restapimodels.response.SessionResponse
import com.google.gson.GsonBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DashBoardPresenterImpl(private var pBasePresenter: MVPView) : DashBoardPresenter {
    override fun getSessionFromApi() {
        getSessions(SessionRequest())
    }

    private fun getSessions(pSessionRequest: SessionRequest) {
            val gson = GsonBuilder().setPrettyPrinting().create().toJson(pSessionRequest)
            Log.e(pSessionRequest.javaClass.name, gson)
            pBasePresenter.showProgress()
            val apiClientConfig = ApiClientConfig()
            val mObservable = apiClientConfig.getRetrofit().create(LogOutNetworkApi::class.java)
                .fetchSessionList(pSessionRequest)
            mObservable.subscribeOn(Schedulers.io()).let {
                it.observeOn(AndroidSchedulers.mainThread())
                it.subscribe(this::handleResponse, this::handleError)
            }
    }

    private fun handleResponse(pLogOutResponseModel: SessionResponse) {
        val gson = GsonBuilder().setPrettyPrinting().create().toJson(pLogOutResponseModel)
        Log.e(pLogOutResponseModel.javaClass.name, gson)
        pBasePresenter.hideProgress()
        pBasePresenter.onSuccess(pLogOutResponseModel)
    }

    private fun handleError(error: Throwable) {
        pBasePresenter.hideProgress()
        pBasePresenter.onError(error.message!!, Constants.ErrorIdentityCode.sAPI_ERROR)
        Log.e(javaClass.name, error.message!!)
    }
}