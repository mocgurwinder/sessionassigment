package com.example.dashBoard.restApiResponse

import com.example.sessionassigment.restapimodels.request.SessionRequest
import com.example.sessionassigment.restapimodels.response.SessionResponse
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

interface LogOutNetworkApi {
    @POST("api/sms-todays-sesslist")
    fun fetchSessionList(@Body pSessionRequest: SessionRequest): Observable<SessionResponse>
}