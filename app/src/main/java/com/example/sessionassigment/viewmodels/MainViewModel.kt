package com.example.sessionassigment.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.application.Applications
import com.example.basePresenter.MVPView
import com.example.sessionassigment.presenter.DashBoardPresenter
import com.example.sessionassigment.presenter.DashBoardPresenterImpl
import com.example.sessionassigment.restapimodels.response.SessionResponse
import com.example.sessionassigment.restapimodels.response.Sessions

class MainViewModel : ViewModel(), MVPView {
     var mMutableList= MutableLiveData<SessionResponse>()
     var mPresenter: DashBoardPresenter = DashBoardPresenterImpl(this)

    override fun showProgress() {
        Applications.mCurrentInstance?.showProgress(Applications.mCurrentInstance?.getmActivity())
    }

    override fun hideProgress() {
        Applications.mCurrentInstance?.hideProgressDialog()
    }

    override fun onSuccess(pResponse: Any) {
        mMutableList.postValue(pResponse as SessionResponse)
    }

    override fun onError(pErrorMessage: String, pType: Int) {

    }

}
