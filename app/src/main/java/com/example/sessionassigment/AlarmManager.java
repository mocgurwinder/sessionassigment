package com.example.sessionassigment;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import static android.content.Context.NOTIFICATION_SERVICE;

public class AlarmManager extends BroadcastReceiver {
    public static final String ANDROID_CHANNEL_ID = "com.aman.ANDROID";
    public static final String ANDROID_CHANNEL_NAME = "ANDROID CHANNEL";
    public static final String DESCRIPTION = "CHANNEL description ";

    @Override
    public void onReceive(Context context, Intent intent) {
        createNotificationChannel(context);
        Log.d("MyAlarm", "Alarm just fired");

    }
    private void createNotificationChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(ANDROID_CHANNEL_ID, ANDROID_CHANNEL_NAME, importance);
            channel.setDescription(DESCRIPTION);
                    NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channel.getId())
                    .setSmallIcon(R.drawable.ic_access_alarm_black_24dp)
                    .setContentTitle("Alert !!! Missed your class")
                    .setContentText("Hurry up !!! Punch in your class,running 5 minutes late")
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    // Set the intent that will fire when the user taps the notification
                  //  .setContentIntent(pendingIntent)
                    .setAutoCancel(true);
            notificationManager.notify(1, builder.build());

        }
    }


}
