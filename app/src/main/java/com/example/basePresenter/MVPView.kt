package com.example.basePresenter


interface MVPView {
    fun showProgress()

    fun hideProgress()

    fun onSuccess(pResponse: Any)

    fun onError(pErrorMessage: String, pType: Int)
}